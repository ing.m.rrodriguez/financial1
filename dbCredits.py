from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Float, Date, Boolean
import pandas as pd
import datetime

from datetime import datetime

from flask import jsonify, request

def configuracionDb():
    # Configura base de datos
    servername = 'DESKTOP-FEOFLS1'
    dbname = 'pruebaIntelligential'
    engine = create_engine('mssql+pyodbc://@' + servername + '/' + dbname + '?trusted_connection=yes&driver=SQL Server Native Client 11.0')
    connection = engine.connect()
    print('Inicia base de datos')
    data = pd.read_sql_query("SELECT TOP (1000) [id_pago],[id_contrato],[id_cliente],[fecha],[monto],[activo],[fecha_registro] FROM [pruebaIntelligential].[dbo].[TableFinanciera]", connection)
    print(data)
    
def consulta(data, id_client, connection):

    print("Consulta: ")
    print(data)
    print(id_client)
    print(connection)

    data = pd.read_sql_query("SELECT TOP (200) [id_pago],[id_contrato],[id_cliente],[fecha],[monto],[activo],[fecha_registro] FROM [dbo].[TableFinanciera]", connection)
    
    cliente = data.loc[data['id_cliente'] == int(id_client)]
    print(cliente)
    cliente = cliente.loc[(cliente.activo)]
    
    #print(cliente)

    return cliente.sort_values('fecha').to_json()
    #return data.to_json()
    
def eliminar(data, engine, id_pago):
    
    
    exist = data.loc[data.id_pago == int(id_pago)]
    
    print("Eliminación de registro")
    
    if not (exist.empty):
        data = data.drop([exist.index[0]])
        data.reset_index(drop=True, inplace=True)
        data = data.sort_index()
        data.to_sql('TableFinanciera',engine, if_exists="replace")
        return jsonify({
        'message': 'Registro ' + id_pago + ' eliminado exitosamente'
        })
    else:
        return jsonify({
        'message': 'Error, el registro ' + id_pago + ' no existe'
        }), 400
    
    
def registrar(data, engine, id_cliente, id_contrato, monto, fecha):
    
    activo = True
    fecha_registro = datetime.now()
    
    id_pago = data.id_pago.max() + 1;
    
    print(fecha_registro)

    nuevo_registro = pd.Series([id_pago, id_contrato, id_cliente, fecha, monto, activo, fecha_registro])
    
    print(nuevo_registro)
    
    data['fecha'] = pd.to_datetime(data['fecha'], format='%Y-%m-%d')

    fecha_filtrada = data.loc[(data['fecha'] > fecha)]

    if fecha_filtrada.empty:
        print('Nuevo pago')
        
        #Agrega registros de nuevos pagos
        
        data.loc[-1]= (list(nuevo_registro))
        data.index = data.index + 1
        data = data.sort_index()
        sql = "update table "
        data.to_sql('TableFinanciera',engine, if_exists="replace")
        
        print(data)
        
    else:
        
        print('Registra pago rezagado')
        
        for n in fecha_filtrada.index:
        
            # Desactivación de pagos remanentes
            
            registro_antiguo = fecha_filtrada.loc[n]
            registro_antiguo.activo = False
            data.loc[n]= (list(registro_antiguo))
            print(registro_antiguo)

        for n in fecha_filtrada.index:
            
            # Aplicación de nuevos pagos
            
            actualiza_registros = fecha_filtrada.loc[n]
            actualiza_registros.activo = True
            actualiza_registros.id_pago = id_pago
            actualiza_registros.fecha_registro = fecha_registro
            id_pago = id_pago + 1
            data.loc[-1]= (list(actualiza_registros))
            data.index = data.index + 1
            data = data.sort_index()
        
        # Inserta nuevo registro
        
        data.loc[data.index[-1] + 1]= (list(nuevo_registro))
        data.to_sql('TableFinanciera',engine, if_exists="replace")
        
        print(data)
    
    return jsonify({"message": "El registro ha sido insertado correctamente"})
    