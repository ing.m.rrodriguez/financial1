from flask import Flask, jsonify, request

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Float, Date, Boolean
import pandas as pd
import datetime

app = Flask(__name__)

servername = 'DESKTOP-FEOFLS1'
dbname = 'pruebaIntelligential'
engine = create_engine('mssql+pyodbc://@' + servername + '/' + dbname + '?trusted_connection=yes&driver=SQL Server Native Client 11.0')
connection = engine.connect()
print('Inicia base de datos')

data = None

data = pd.read_sql_query("SELECT TOP (1000) [id_pago],[id_contrato],[id_cliente],[fecha],[monto],[activo],[fecha_registro] FROM [dbo].[TableFinanciera]", connection)
print(data)

#from dbCredits import configuracionDb
from dbCredits import consulta
from dbCredits import eliminar
from dbCredits import registrar



# Testing Route
@app.route('/ping', methods=['GET'])
def ping():
    return jsonify({'response': 'pong!'})

# Get Data
@app.route('/payments/<string:id_client>')
def getProducts(id_client):
    return consulta(data, id_client, connection)

# Delete Data
@app.route('/payments/<string:id_pago>', methods=['DELETE'])
def deleteProduct(id_pago):
    print(id_pago)
    return eliminar(data, engine, id_pago)
    
# Registrar pago
# Create Data Routes
@app.route('/payments/<string:clientId>', methods=['POST'])
def addProduct(clientId):
    new_registry = {
        'id_contrato': request.json['id_contrato'],
        'fecha': request.json['fecha'],
        'monto': request.json['monto'],
    }

    
    id_contrato = new_registry.get('id_contrato')
    monto = new_registry.get('monto')
    fecha = new_registry.get('fecha')
    
    return registrar(data, engine, clientId, id_contrato, monto, fecha)

if __name__ == '__main__':
    #configuracionDb()
    print('Inicia app')
    app.run(debug=True, port=4000)
